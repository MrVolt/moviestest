package com.dev.moviestest

import android.view.View
import com.dev.moviestest.data.datasources.api.models.PaginatedPopularMoviesResponse
import com.dev.moviestest.domain.models.Movie
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList

/**
 * Method to set view visibility to gone
 */
fun View.gone() {
    if (this.visibility != View.GONE) this.visibility = View.GONE
}

/**
 * Method to set view visibility to visible
 */
fun View.visible() {
    if (this.visibility != View.VISIBLE) this.visibility = View.VISIBLE
}

/** region Map functions */
fun PaginatedPopularMoviesResponse.toModel() =
    PaginatedPopularMoviesList(
        lastPage = totalPages,
        movies = results.map { it.toModel() }
    )

fun com.dev.moviestest.data.datasources.api.models.Movie.toModel() =
    Movie(
        overview,
        title,
        posterPath,
        releaseDate ?: "",
        voteCount
    )
/** endregion */
