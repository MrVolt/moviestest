package com.dev.moviestest.domain.models

data class PaginatedPopularMoviesList(
    val lastPage: Int? = null,
    val movies: List<Movie>? = null
)