package com.dev.moviestest.domain.models

data class Movie(
    val overview: String = "",
    val title: String = "",
    val posterPath: String = "",
    val releaseDate: String = "",
    val voteCount: Int = 0
)
