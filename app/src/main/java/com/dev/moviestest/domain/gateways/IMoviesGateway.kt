package com.dev.moviestest.domain.gateways

import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import io.reactivex.Single

interface IMoviesGateway {
    fun popularMovies(currentPage: Int): Single<PaginatedPopularMoviesList>
}