package com.dev.moviestest.domain.interactors

import com.dev.moviestest.domain.gateways.IMoviesGateway
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.SingleObserver

class MoviesInteractor(
    private var moviesGateway: IMoviesGateway
) : BaseInteractor() {
    fun popularMovies(currentPage: Int, observer: SingleObserver<PaginatedPopularMoviesList>) {
        subscribeSingle(ReactiveNetwork.checkInternetConnectivity().flatMap {
            if (it) moviesGateway.popularMovies(currentPage)
            else Single.just(PaginatedPopularMoviesList())
        }, observer)
    }

    fun checkInternetAndRetry(
        processingPage: Int,
        observer: Observer<PaginatedPopularMoviesList>
    ) {
        subscribeObservable(ReactiveNetwork.observeInternetConnectivity().flatMap {
            if (it) moviesGateway.popularMovies(processingPage).toObservable()
            else Observable.just(PaginatedPopularMoviesList())
        }, observer)
    }
}