package com.dev.moviestest.domain.interactors

import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


abstract class BaseInteractor {
    protected fun <T> subscribeSingle(single: Single<T>, singleObserver: SingleObserver<T>) {
        subscribeSingle(single, Schedulers.io(), AndroidSchedulers.mainThread(), singleObserver)
    }

    private fun <T> subscribeSingle(
        single: Single<T>,
        subscribeOn: Scheduler,
        observeOn: Scheduler,
        singleObserver: SingleObserver<T>
    ) {
        execute(single, subscribeOn, observeOn).subscribe(singleObserver)
    }

    private fun <T> execute(
        single: Single<T>,
        subscribeOn: Scheduler,
        observeOn: Scheduler
    ): Single<T> {
        return single
            .subscribeOn(subscribeOn)
            .observeOn(observeOn)
    }

    protected fun <T> subscribeObservable(observable: Observable<T>, observer: Observer<T>) {
        subscribeObservable(observable, Schedulers.io(), AndroidSchedulers.mainThread(), observer)
    }

    private fun <T> subscribeObservable(
        observable: Observable<T>,
        subscribeOn: Scheduler,
        observeOn: Scheduler,
        observer: Observer<T>
    ) {
        observable
            .observeOn(observeOn)
            .subscribeOn(subscribeOn)
            .subscribe(observer)
    }
}
