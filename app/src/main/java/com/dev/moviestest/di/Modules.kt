package com.dev.moviestest.di

import android.content.Context
import android.content.SharedPreferences
import com.dev.moviestest.BuildConfig
import com.dev.moviestest.data.datasources.IMoviesDS
import com.dev.moviestest.data.datasources.api.RetrofitManager
import com.dev.moviestest.data.datasources.api.RetrofitMoviesDS
import com.dev.moviestest.data.gateways.MoviesGateway
import com.dev.moviestest.domain.gateways.IMoviesGateway
import com.dev.moviestest.domain.interactors.MoviesInteractor
import com.dev.moviestest.presentation.activities.main.Presenter
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val appModule = module(override = true) {
    single<SharedPreferences> {
        androidContext().getSharedPreferences(
            BuildConfig.APPLICATION_ID,
            Context.MODE_PRIVATE
        )
    }

    single { MoviesInteractor(get()) }

    single<IMoviesGateway> { MoviesGateway(get()) }

    single<IMoviesDS> { RetrofitMoviesDS(get()) }

    single { RetrofitManager() }

    factory { Presenter() }
    factory { com.dev.moviestest.presentation.fragments.main.Presenter(get()) }
}