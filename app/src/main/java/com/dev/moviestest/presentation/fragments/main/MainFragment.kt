package com.dev.moviestest.presentation.fragments.main

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.dev.moviestest.PAGE_START
import com.dev.moviestest.R
import com.dev.moviestest.databinding.FragmentMainBinding
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import com.dev.moviestest.presentation.adapters.PopularMoviesAdapter
import com.dev.moviestest.presentation.fragments.BaseFragment
import com.dev.moviestest.presentation.viewComponents.PaginationListener
import org.koin.android.ext.android.inject

class MainFragment : BaseFragment<Presenter.View, Presenter>(), Presenter.View {
    private lateinit var popularMoviesAdapter: PopularMoviesAdapter
    private lateinit var binding: FragmentMainBinding

    private val viewPresenter: Presenter by inject()

    override val contentView = R.layout.fragment_main

    override fun createPresenter() = viewPresenter

    private var currentPage = PAGE_START
    private var isLastPage = false
    private var isLoading = false

    override fun onInitViews(view: View) {
        binding = FragmentMainBinding.bind(view)

        initList()

        presenter.getPopularMovies(currentPage)
    }

    private fun initList() {
        popularMoviesAdapter = PopularMoviesAdapter()

        val linearLayoutManager = LinearLayoutManager(context)

        binding.moviesRV.layoutManager = linearLayoutManager
        binding.moviesRV.adapter = popularMoviesAdapter

        binding.moviesRV.addOnScrollListener(object : PaginationListener(linearLayoutManager) {
            override fun loadMoreItems() {
                isLoading = true
                currentPage++
                presenter.getPopularMovies(currentPage)
            }

            override fun isLastPage() = isLastPage

            override fun isLoading() = isLoading
        })
    }

    override fun displayPopularMovies(data: PaginatedPopularMoviesList) {
        if (currentPage != PAGE_START) popularMoviesAdapter.removeLoading()
        popularMoviesAdapter.addProducts(data.movies!!)

        if (currentPage < data.lastPage!!) {
            popularMoviesAdapter.addLoading()
        } else {
            isLastPage = true
        }
        isLoading = false
    }

    override fun showNoInternetMessage() {
        // For better user experience we need to show message BEFORE we start to observe internet availability
        Toast.makeText(requireContext(), getString(R.string.no_internet), Toast.LENGTH_LONG).show()

        presenter.checkInternetAndRetry()
    }
}