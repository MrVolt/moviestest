package com.dev.moviestest.presentation

import com.hannesdorfmann.mosby3.mvp.MvpView

interface BaseView : MvpView {
    fun displayError(throwable: Throwable)
    fun showProgress()
    fun hideProgress()
    fun displayMessage(message: String)
}
