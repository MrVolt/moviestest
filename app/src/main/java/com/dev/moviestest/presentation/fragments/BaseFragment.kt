package com.dev.moviestest.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import com.dev.moviestest.presentation.BasePresenter
import com.dev.moviestest.presentation.BaseView
import com.dev.moviestest.presentation.ProgressView
import com.hannesdorfmann.mosby3.mvp.MvpFragment

abstract class BaseFragment<V : BaseView, P : BasePresenter<V>> : MvpFragment<V, P>(), BaseView {
    open var savedInstanceState: Bundle? = null

    open val progressView = object : ProgressView {
        override fun showProgress() {
            (requireActivity() as BaseView).showProgress()
        }

        override fun hideProgress() {
            (requireActivity() as BaseView).hideProgress()
        }
    }

    @get:LayoutRes
    internal abstract val contentView: Int

    private val navController: NavController
        get() = NavHostFragment.findNavController(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.savedInstanceState = savedInstanceState
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return initContentView(inflater, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInitViews(view)
    }

    private fun initContentView(inflater: LayoutInflater, container: ViewGroup?): View {
        return inflater.inflate(contentView, container, false)
    }

    open fun onInitViews(view: View) {}

    fun navigateUp() {
        navController.navigateUp()
    }

    fun navigateTo(@IdRes resId: Int) {
        navController.navigate(resId)
    }

    fun navigateTo(@IdRes resId: Int, args: Bundle) {
        navController.navigate(resId, args)
    }

    fun navigateTo(directions: NavDirections) {
        navController.navigate(directions)
    }

    fun openDialog(dialogFragment: DialogFragment?) {
        dialogFragment?.show(childFragmentManager, dialogFragment.javaClass.name)
    }

    override fun displayError(throwable: Throwable) {
        showToast(throwable.message)
    }

    override fun showProgress() {
        progressView.showProgress()
    }

    override fun hideProgress() {
        progressView.hideProgress()
    }

    override fun displayMessage(message: String) {
        showToast(message)
    }

    private fun showToast(text: String?) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }
}
