package com.dev.moviestest.presentation

interface ProgressView {
    fun showProgress()
    fun hideProgress()
}
