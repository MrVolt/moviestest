package com.dev.moviestest.presentation.utils

import android.widget.ImageView
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.dev.moviestest.BuildConfig

@GlideModule
class ImageProcessor : AppGlideModule() {
    /**
     * Method to just load image by url
     *
     * @param url       - image url
     * @param imageView - container
     */
    fun load(url: String, imageView: ImageView) {
        GlideApp.with(imageView.context)
            .load(BuildConfig.IMAGES_URL + url)
            .into(imageView)
    }
}
