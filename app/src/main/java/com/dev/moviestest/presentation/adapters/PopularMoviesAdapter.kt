package com.dev.moviestest.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dev.moviestest.databinding.ItemMovieBinding
import com.dev.moviestest.databinding.ItemProgressBinding
import com.dev.moviestest.domain.models.Movie
import com.dev.moviestest.presentation.utils.ImageProcessor

class PopularMoviesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val data: MutableList<Movie> = mutableListOf()
    private var isLoaderVisible = false

    private val viewTypeLoading = 0
    private val viewTypeNormal = 1

    override fun getItemViewType(position: Int): Int {
        return if (isLoaderVisible) {
            if (position == data.size - 1) viewTypeLoading else viewTypeNormal
        } else {
            viewTypeNormal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            viewTypeNormal -> MovieViewHolder(
                ItemMovieBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
            else -> ProgressHolder(
                ItemProgressBinding.inflate(
                    LayoutInflater.from(
                        parent.context
                    ), parent, false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            holder.bind(data[position])
        }
    }

    override fun getItemCount() = data.size

    fun removeLoading() {
        isLoaderVisible = false
        val position: Int = data.size - 1
        if (position != -1) {
            data.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun addProducts(postItems: List<Movie>) {
        data.addAll(postItems)
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaderVisible = true
        data.add(Movie())
        notifyItemInserted(data.size - 1)
    }

    class MovieViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Movie) {
            binding.titleTV.text = item.title
            binding.descriptionTV.text = item.overview
            binding.ratingTV.text = item.voteCount.toString()

            ImageProcessor().load(item.posterPath, binding.posterIV)
        }
    }

    class ProgressHolder(binding: ItemProgressBinding) :
        RecyclerView.ViewHolder(binding.root)
}