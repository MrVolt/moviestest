package com.dev.moviestest.presentation

import com.hannesdorfmann.mosby3.mvp.MvpNullObjectBasePresenter
import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

open class BasePresenter<V : BaseView> : MvpNullObjectBasePresenter<V>() {
    private var compositeDisposable: CompositeDisposable? = null

    override fun detachView() {
        super.detachView()
        dispose()
    }

    private fun showProgress() {
        view.showProgress()
    }

    private fun hideProgress() {
        view.hideProgress()
    }

    private fun displayError(throwable: Throwable) {
        view.displayError(throwable)
    }

    private fun addDisposable(disposable: Disposable) {
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
        }
        compositeDisposable!!.add(disposable)
    }

    private fun dispose() {
        if (compositeDisposable != null) {
            compositeDisposable!!.dispose()
            compositeDisposable = null
        }
    }

    fun disposeSingle(disposable: Disposable?) {
        if (disposable != null && !disposable.isDisposed) {
            disposable.dispose()
        }
    }

    open inner class ProgressSingleObserver<T> : SingleObserver<T> {

        override fun onSubscribe(disposable: Disposable) {
            showProgress()
            addDisposable(disposable)
        }

        override fun onSuccess(data: T) {
            Timber.d("onSuccess")

            hideProgress()
        }

        override fun onError(throwable: Throwable) {
            Timber.d("onError")

            hideProgress()
            displayError(throwable)
        }
    }

    open inner class ProgressObservable<T> : Observer<T> {

        override fun onSubscribe(disposable: Disposable) {
            addDisposable(disposable)
            showProgress()
        }

        override fun onNext(data: T) {
            Timber.d("onNext")

            hideProgress()
        }

        override fun onError(throwable: Throwable) {
            Timber.d("onError")

            hideProgress()
            displayError(throwable)
        }

        override fun onComplete() {
            Timber.d("onComplete")

            hideProgress()
        }
    }
}
