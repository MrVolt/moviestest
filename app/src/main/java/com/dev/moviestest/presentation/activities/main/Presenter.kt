package com.dev.moviestest.presentation.activities.main

import com.dev.moviestest.presentation.BasePresenter
import com.dev.moviestest.presentation.BaseView

class Presenter : BasePresenter<Presenter.View>() {
    interface View : BaseView
}