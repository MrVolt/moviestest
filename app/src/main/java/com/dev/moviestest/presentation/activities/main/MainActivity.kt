package com.dev.moviestest.presentation.activities.main

import androidx.navigation.fragment.NavHostFragment
import com.dev.moviestest.R
import com.dev.moviestest.databinding.ActivityMainBinding
import com.dev.moviestest.gone
import com.dev.moviestest.presentation.ProgressView
import com.dev.moviestest.presentation.activities.BaseActivity
import com.dev.moviestest.visible
import org.koin.android.ext.android.inject

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivity : BaseActivity<Presenter.View, Presenter>(), Presenter.View {
    private val viewPresenter: Presenter by inject()

    private lateinit var binding: ActivityMainBinding

    override val contentView: Int = R.layout.activity_main

    override fun createPresenter(): Presenter {
        return viewPresenter
    }

    override val progressView = object : ProgressView {
        override fun showProgress() {
            binding.progressBar.visible()
        }

        override fun hideProgress() {
            binding.progressBar.gone()
        }
    }

    @Suppress("DEPRECATION")
    override fun onInitViews() {
        binding = ActivityMainBinding.inflate(layoutInflater)

        initNavController()
    }

    private fun initNavController() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.navigationHost) as NavHostFragment
        navController = navHostFragment.navController

        if (previousState == null) {
            navController.setGraph(R.navigation.main_graph)
        }

        navController.addOnDestinationChangedListener { _, destination, _ ->

        }
    }
}
