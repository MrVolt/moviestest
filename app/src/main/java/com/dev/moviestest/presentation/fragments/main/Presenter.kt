package com.dev.moviestest.presentation.fragments.main

import com.dev.moviestest.domain.interactors.MoviesInteractor
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import com.dev.moviestest.presentation.BasePresenter
import com.dev.moviestest.presentation.BaseView
import timber.log.Timber

class Presenter(private val moviesInteractor: MoviesInteractor) : BasePresenter<Presenter.View>() {
    private var processingPage = 0

    fun getPopularMovies(currentPage: Int) {
        Timber.d("getPopularMovies page %s", currentPage)

        processingPage = currentPage

        moviesInteractor.popularMovies(currentPage, object :
            ProgressSingleObserver<PaginatedPopularMoviesList>() {
            override fun onSuccess(data: PaginatedPopularMoviesList) {
                if (data.movies == null)
                    view.showNoInternetMessage()
                else
                    view.displayPopularMovies(data)
            }
        })
    }

    fun checkInternetAndRetry() {
        Timber.d("checkInternetAndRetry")

        moviesInteractor.checkInternetAndRetry(processingPage, object :
            ProgressObservable<PaginatedPopularMoviesList>() {
            override fun onNext(data: PaginatedPopularMoviesList) {
                if (data.movies != null) {
                    view.displayPopularMovies(data)

                    super.onNext(data)
                }
            }
        })
    }

    interface View : BaseView {
        fun displayPopularMovies(data: PaginatedPopularMoviesList)
        fun showNoInternetMessage()
    }
}