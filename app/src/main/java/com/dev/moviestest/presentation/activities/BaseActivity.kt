package com.dev.moviestest.presentation.activities

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.navigation.NavController
import com.dev.moviestest.presentation.BasePresenter
import com.dev.moviestest.presentation.BaseView
import com.dev.moviestest.presentation.ProgressView
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import java.util.*

abstract class BaseActivity<V : BaseView, P : BasePresenter<V>> : MvpActivity<V, P>(), BaseView {
    var previousState: Bundle? = null

    @get:LayoutRes
    abstract val contentView: Int

    lateinit var navController: NavController

    open val progressView: ProgressView
        get() = object : ProgressView {
            override fun showProgress() {

            }

            override fun hideProgress() {

            }
        }

    @TargetApi(Build.VERSION_CODES.N)
    private fun updateResourcesLocale(
        context: Context,
        locale: Locale
    ): Context? {
        val configuration =
            context.resources.configuration
        configuration.setLocale(locale)
        return context.createConfigurationContext(configuration)
    }

    @Suppress("DEPRECATION")
    private fun updateResourcesLocaleLegacy(
        context: Context,
        locale: Locale
    ): Context {
        val resources = context.resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)
        return context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        previousState = savedInstanceState

        initContent()
        onInitViews()
    }

    private fun initContent() {
        setContentView(contentView)
    }

    open fun onInitViews() {}

    override fun displayError(throwable: Throwable) {
        showToast(throwable.message)
    }

    override fun showProgress() {
        progressView.showProgress()
    }

    override fun hideProgress() {
        progressView.hideProgress()
    }

    override fun displayMessage(message: String) {
        showToast(message)
    }

    private fun showToast(msg: String?) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    fun navigateTo(direction: Int) {
        navController.navigate(direction)
    }

    fun navigateUp() {
        navController.navigateUp()
    }
}
