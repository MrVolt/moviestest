package com.dev.moviestest.data.gateways

import com.dev.moviestest.data.datasources.IMoviesDS
import com.dev.moviestest.domain.gateways.IMoviesGateway
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import io.reactivex.Single

class MoviesGateway(private var retrofitMoviesDS: IMoviesDS) : IMoviesGateway {
    override fun popularMovies(currentPage: Int): Single<PaginatedPopularMoviesList> {
        return retrofitMoviesDS.popularMovies(currentPage)
    }
}