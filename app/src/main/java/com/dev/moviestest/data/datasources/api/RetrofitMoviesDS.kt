package com.dev.moviestest.data.datasources.api

import com.dev.moviestest.BuildConfig
import com.dev.moviestest.data.datasources.IMoviesDS
import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import com.dev.moviestest.toModel
import io.reactivex.Single

class RetrofitMoviesDS(private var retrofitManager: RetrofitManager) : IMoviesDS {
    override fun popularMovies(currentPage: Int): Single<PaginatedPopularMoviesList> {
        return retrofitManager.api.popularMovies(BuildConfig.API_KEY, currentPage).map {
            it.toModel()
        }
    }
}