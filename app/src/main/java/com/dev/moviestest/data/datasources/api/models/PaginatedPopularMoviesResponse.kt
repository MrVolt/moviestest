package com.dev.moviestest.data.datasources.api.models

import com.google.gson.annotations.SerializedName

data class PaginatedPopularMoviesResponse(
    @field:SerializedName("page")
    val page: Int,

    @field:SerializedName("total_pages")
    val totalPages: Int,

    @field:SerializedName("results")
    val results: List<Movie>,

    @field:SerializedName("total_results")
    val totalResults: Int
)