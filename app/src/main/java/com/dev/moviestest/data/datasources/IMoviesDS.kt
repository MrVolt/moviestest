package com.dev.moviestest.data.datasources

import com.dev.moviestest.domain.models.PaginatedPopularMoviesList
import io.reactivex.Single

interface IMoviesDS {
    fun popularMovies(currentPage: Int): Single<PaginatedPopularMoviesList>
}