package com.dev.moviestest.data.datasources.api

import com.dev.moviestest.data.datasources.api.models.PaginatedPopularMoviesResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitApi {
    /**region Movies*/

    @GET("movie/popular")
    fun popularMovies(
        @Query("api_key") apiKey: String,
        @Query("page") page: Int
    ): Single<PaginatedPopularMoviesResponse>

    /**endregion*/
}