plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("androidx.navigation.safeargs.kotlin")
}

val versionMajor = 1
val versionMinor = 0
val versionFix = 0

android {
    compileSdkVersion(30)

    defaultConfig {
        applicationId = "com.dev.moviestest"

        minSdkVersion(23)
        targetSdkVersion(30)
        versionCode = (versionMajor * 1000000 + versionMinor * 1000 + versionFix) + 2010000000
        versionName = "$versionMajor.$versionMinor.$versionFix"
        setProperty("archivesBaseName", "$applicationId-$versionName")

        buildConfigField("String", "BASE_URL", "\"https://api.themoviedb.org/3/\"")
        buildConfigField("String", "API_KEY", "\"f0061fa8a205acb974e61b45cc251e37\"")
        buildConfigField("String", "IMAGES_URL", "\"https://image.tmdb.org/t/p/w500\"")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        getByName("debug") {
            isDebuggable = true
            applicationIdSuffix = ".debug"
            versionNameSuffix = "-debug"
        }
    }

    buildFeatures.viewBinding = true

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    // Android
    implementation("androidx.appcompat:appcompat:1.3.0")
    implementation("androidx.core:core-ktx:1.6.0-beta01")

    // Android.UI
    implementation("com.google.android.material:material:1.3.0")
    implementation("androidx.recyclerview:recyclerview:1.2.0")
    implementation("androidx.constraintlayout:constraintlayout:2.0.4")

    // Navigation
    implementation("androidx.navigation:navigation-fragment-ktx:2.3.5")
    implementation("androidx.navigation:navigation-ui-ktx:2.3.5")

    // RX.Core
    implementation("io.reactivex.rxjava2:rxandroid:2.1.1")
    implementation("io.reactivex.rxjava2:rxjava:2.2.21")

    // RX.Connectivity
    implementation("com.github.pwittchen:reactivenetwork-rx2:3.0.8")

    // MVP
    val mosby = "3.1.1"
    implementation("com.hannesdorfmann.mosby3:mvp:$mosby")
    implementation("com.hannesdorfmann.mosby3:mvp-nullobject-presenter:$mosby")

    // Image
    val glide = "4.12.0"
    implementation("com.github.bumptech.glide:glide:$glide")
    kapt("com.github.bumptech.glide:compiler:$glide")

    // Network
    val retrofit = "2.9.0"
    implementation("com.squareup.retrofit2:retrofit:$retrofit")
    implementation("com.squareup.retrofit2:adapter-rxjava2:$retrofit")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit")
    implementation("com.squareup.okhttp3:logging-interceptor:5.0.0-alpha.2")

    // Serialization
    implementation("com.google.code.gson:gson:2.8.7")

    // Logger
    implementation("com.jakewharton.timber:timber:4.7.1")

    // DI Koin
    implementation("io.insert-koin:koin-android:3.0.2")
}
